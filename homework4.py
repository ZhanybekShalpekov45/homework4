class Name:
    def __init__(self,name):
        self.name = name

class Age:
    def __init__(self,age):
        self.__age = age
    @property
    def age(self):
        return self.__age
    @age.setter
    def age(self, age):
        if age < 0:
            raise ValueError("возраст должен быть положительным")
        self.__age = age

class Acquaintance:
     def greeting(self):
         return f' Hello {self.name}!'
class Age_info:
    def age_info(self):
        return f'I am {self.age} years old'
class Farewall(Name,Age,Acquaintance,Age_info):
    def __init__(self,name,age):
        Name.__init__(self,name)
        Age.__init__(self,age)
object = Farewall('maks',14)
object.greeting()
object.age_info()